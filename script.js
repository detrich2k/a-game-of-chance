const result_div = document.querySelector(".result");
const Rock_div = document.getElementById("rock");
const Paper_div = document.getElementById("paper");
const Scissors_div = document.getElementById("scissors");
let choice = ["rock", "paper", "sciccors"]

function getRandomInt() {
    const choices = ["r", "p", "s"];
    const randomNumber = Math.floor(Math.random() * Math.floor(3));
    return choices[randomNumber]
}

function convertToWord(letter) {
    if (letter === "r") return "Rock🗿";
    if (letter === "p") return "Paper📰";
    else return "Sciccors✂️"
}

function win(user, computer) {
    result_div.innerHTML = convertToWord(user) + " beats " + convertToWord(computer) + "<span > You Win!!!!!🔥</span>"
}

function lose(user, computer) {
    result_div.innerHTML = convertToWord(user) + " loses to " + convertToWord(computer) + "<span> You Lose!😦</span>"
}

function tie(user) {
    result_div.innerHTML = "Both players chose " + convertToWord(user) + "<span> It's a DRAW! Choose again!💯</span>"
}

function game(userChoice) {
    const computerChoice = getRandomInt();
    switch (userChoice + computerChoice) {
        case "rs":
        case "pr":
        case "sp":
            win(userChoice, computerChoice);
            break;
        case "rp":
        case "ps":
        case "sr":
            lose(userChoice, computerChoice);
            break;
        case "rr":
        case "pp":
        case "ss":
            tie(userChoice, computerChoice);
            break;

    }
}

function main() {
    Rock_div.addEventListener('click', function() {
        game("r")
    })
    Paper_div.addEventListener('click', function() {
        game("p")
    })
    Scissors_div.addEventListener('click', function() {
        game("s")
    })
}
main();